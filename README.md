# Hands-on ML

A learning project to explore and implement textbook Machine Learning problems

## List of Implemented ML Solutions

### 1. Image Recognition using Neural Networks

Implementation of a Convolutional Neural Network on the [CIFAR-10 Dataset](http://www.cs.toronto.edu/~kriz/cifar.html) using Keras.

The CIFAR-10 dataset contains 60,000 32x32 color images in 10 different classes, with 6000 images per class. 

### 2. Semantic Analysis with VADER and Transformers

Implementing Sentiment Analysis on a [Sentiment Labelled Sentences](https://archive.ics.uci.edu/ml/machine-learning-databases/00331/) dataset from the open-source UCI Machine Learning Repository using VADER and Transformers.

### 3. Face Recognition with PIL and Face_recognition

Implementation of a Face Recognition on test images using PIL and Face Recognition.

The images used for training and testing are from the Women Engineers Bootcamp. 

### 4. Abstractive Text Summarization with Transformers

Implementing Text Summarization on an article from [Brittanica](https://www.britannica.com/science/global-warming) using Transformers.

## Toolkit

<img src="https://cdn.svgporn.com/logos/python.svg" width=52> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Google_Colaboratory_SVG_Logo.svg/1200px-Google_Colaboratory_SVG_Logo.svg.png?20221103151432" width=100>
<img src="https://cdn.svgporn.com/logos/gitlab.svg" width=52> 




